/*
 * Copyright 2015-2018 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */

package com.example.project;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class FineCalculatorTest {

	@Test
	@DisplayName("10kmh too fast")
	void calculateWith10kmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(10, calculator.getFine(10), "Fine for 10 km/h should be 10 EUROS");
	}

	
	@Test
	@DisplayName("0kmh too fast")
	void calculateWithNokmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(0, calculator.getFine(0), "No Fine");
	}

}
